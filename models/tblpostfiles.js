'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'last_updated'
    },
    versionKey: false
};

let tblpostfilesSchema = new Schema({
    userid: {
        type: String
    },
    postid: {
        type: String
    },
    type: {
        type: String
    },
    file: {
        type: String
    },

}, schemaOptions)


var tblpostfiles = mongoose.model('tblpostfiles', tblpostfilesSchema);
module.exports = tblpostfiles;