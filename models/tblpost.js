'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'last_updated'
    },
    versionKey: false
};

let tblpostSchema = new Schema({
    userid: {
        type: String
    },
    like: {
        type: Number,
        default: 0
    },
    caption: {
        type: String
    },
    location: {
        type: String
    },
    //public or private
    type: {
        type: String
    },
    tagpeople: {
        type: Array
    }

}, schemaOptions)


var tblpost = mongoose.model('tblpost', tblpostSchema);
module.exports = tblpost;