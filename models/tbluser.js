const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const schemaOptions = {
  timestamps: {
    createdAt: "created_at",
    updatedAt: "last_updated",
  },
  versionKey: false,
};

let tbluserSchema = new Schema(
  {
    uniqueid: {
      type: String,
      default: "",
      required: true
    },
    fullname: {
      type: String,
      default: "",
      required: true
    },
    accesstoken: {
      type: String,
      default: ""
    },
    password: {
      type: String,
      default: ""
    },
    phone: {
      type: String,
      required: true
    },
    facebookid: {
      type: String
    },
    email: {
      type: String
    },
    followers: {
      type: Number,
      default: 0,
    },
    following: {
      type: Number,
      default: 0,
    },
    gender: {
      type: String,
      required: true
    },
    dob: {
      type: String
    },
    country: {
      type: Number,
    },
    state: {
      type: Number,
    },
    city: {
      type: Number,
    },
    latitude: {
      type: String,
      required: true
    },
    longitude: {
      type: String,
      required: true
    },
    age: {
      type: String
    },
    profilepic: {
      type: String
    },
    hobbies: {
      type: Array
    },
    bio: {
      type: String
    },
    genderinterest: {
      type: Array
    },
    favfood: {
      type: String
    },
    favcolor: {
      type: String
    },
    agepreference: {
      type: Array
    },
    lookingfor: {
      type: String
    },
    study: {
      type: String
    },
    work: {
      type: String
    },
    roleid: {
      type: String,
    },
    issuspended: {
      type: Number,
      default: 0,
    },
    isdeleted: {
      type: Number,
      default: 0
    },
    isprivate: {
      type: Number,
      default: 0
    },
    website: {
      type: String,
    },
    qrimage: {
      type: String,
    },
    createddate: {
      type: String,
      default: "",
    },
  },
  schemaOptions
);

var tbluser = mongoose.model("tbluser", tbluserSchema);
module.exports = tbluser;
