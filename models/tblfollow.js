const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
    versionKey: false
};

let tblfollowSchema = new Schema({
    whofollow: {
        type: String,
    },
    whomfollow: {
        type: String,
    },
    status: {
        type: Number,
        default: 0,
    }
}, schemaOptions)


var tblfollow = mongoose.model('tblfollow', tblfollowSchema);
module.exports = tblfollow;