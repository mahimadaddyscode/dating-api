
const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblroleSchema = new Schema({
  rolename: {
    type: String,
    default: ''
  },
  description: {
    type: String,
    default: ''
  },
  type: {
    type: String,
    default: ''
  },
  isdeleted: {
    type: Number,
    default: 0
  }
}, schemaOptions)

var tblrole = mongoose.model('tblrole', tblroleSchema);
module.exports = tblrole;