'use strict'

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblmoduleSchema = new Schema({
  id: {
    type: Number,
    // enables us to search the record faster
    index: true,
    unique: true,
    required: true
  },
  module: {
    type: String,
    default: ''
  },
  isactive: {
    type: Number,
    default: 0
  },
  displayorder: {
    type: Number,
    default: 0
  }
}, schemaOptions)

var tblmodule = mongoose.model('tblmodule', tblmoduleSchema);
module.exports = tblmodule;