const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    versionKey: false
};

let tbluserotpverificationSchema = new Schema({
    phone: {
        type: String
    },
    otp: {
        type: String,
        default: ""
    },
    date: {
        type: String,
        default: ""
    },
    expirydate: {
        type: String,
        default: ""
    }
}, schemaOptions)

var tbluserotpverification = mongoose.model('tbluserotpverification', tbluserotpverificationSchema);
module.exports = tbluserotpverification;