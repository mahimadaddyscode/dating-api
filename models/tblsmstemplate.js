/* jshint indent: 2 */
const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const schemaOptions = {
  versionKey: false
};

let tblsmstemplateSchema = new Schema({
  type: {
    type: String,
    default: ""
  },
  body: {
    type: String,
    default: ""
  }
}, schemaOptions)

var tblsmstemplate = mongoose.model('tblsmstemplate', tblsmstemplateSchema);
module.exports = tblsmstemplate;