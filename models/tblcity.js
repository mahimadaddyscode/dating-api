const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblcitySchema = new Schema({
  city: {
    type: String,
    default: ''
  },
  state: {
    type: String,
  }
}, schemaOptions)

var tblcity = mongoose.model('tblcity', tblcitySchema);
module.exports = mongoose.model('tblcity', tblcitySchema);