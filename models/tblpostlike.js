'use strict'
/**
 * Module Dependencies
 */

const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'last_updated'
    },
    versionKey: false
};

let tblpostlikeSchema = new Schema({
    userid: {
        type: String
    },
    postid: {
        type: String
    },
    like: {
        type: Number,
        default: 0
    }
}, schemaOptions)


module.exports = mongoose.model('tblpostlike', tblpostlikeSchema);