const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const schemaOptions = {
  versionKey: false
};

let tblresetpasswordSchema = new Schema({
  userid: {
    type: Number
  },
  phone: {
    type: Number
  },
  otp: {
    type: String,
    default: ""
  },
  date: {
    type: String,
    default: ""
  },
  expirydate: {
    type: String,
    default: ""
  }
}, schemaOptions)

var tblresetpassword = mongoose.model('tblresetpassword', tblresetpasswordSchema);
module.exports = tblresetpassword;