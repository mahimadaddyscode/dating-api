const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tbluserpermissionSchema = new Schema({
  idmodule: {
    type: Number,
  },
  idrole: {
    type: Number,
  },
  added: {
    type: Number,
    default: 0
  },
  modified: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Number,
    default: 0
  },
  show: {
    type: Number,
    default: 0
  }
}, schemaOptions)

var tbluserpermission = mongoose.model('tbluserpermission', tbluserpermissionSchema);
module.exports = tbluserpermission;