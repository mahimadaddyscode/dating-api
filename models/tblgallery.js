'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'last_updated'
    },
    versionKey: false
};

let tblgallerySchema = new Schema({
    userid: {
        type: String
    },
    type: {
        type: String
    },
    file: {
        type: String
    },

}, schemaOptions)


var tblgallery = mongoose.model('tblgallery', tblgallerySchema);
module.exports = tblgallery;