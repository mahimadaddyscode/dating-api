
'use strict'
/**
 * Module Dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const schemaOptions = {
    timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
    versionKey: false
};

let tblpostcommentSchema = new Schema({
    userid: {
        type: String
    },
    postid: {
        type: String,
    },
    comment: {
        type: String
    },
    referenceid: {
        type: String,
        default: ""
    }
}, schemaOptions)


module.exports = mongoose.model('tblpostcomment', tblpostcommentSchema);