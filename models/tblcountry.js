const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblcountrySchema = new Schema({
  country: {
    type: String,
    default: ''
  }
}, schemaOptions)

module.exports = mongoose.model('tblcountry', tblcountrySchema);