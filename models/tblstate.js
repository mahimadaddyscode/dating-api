const mongoose = require('mongoose'),
  Schema = mongoose.Schema;
const schemaOptions = {
  timestamps: { createdAt: 'created_at', updatedAt: 'last_updated' },
  versionKey: false
};

let tblstateSchema = new Schema({
  state: {
    type: String,
    default: ''
  },
  country: {
    type: String,
  }
}, schemaOptions)

module.exports = mongoose.model('tblstate', tblstateSchema);