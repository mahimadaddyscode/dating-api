var express = require('express');
var router = express.Router();
var moment = require('moment');
var user = require('../../common/user');
var common = require('../../common/common');
var Auth = require('../../middleware/auth');
var smsmail = require('../../common/smsmail');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const User = require('../../models/tbluser');
const Gallery = require('../../models/tblgallery');
const Post = require('../../models/tblpost');
const PostFiles = require('../../models/tblpostfiles');
const ObjectId = require('mongodb').ObjectID;
var formidable = require('formidable');
var fs = require('fs');

//Get user detail by token
router.post('/GetUserdetailsbyuserId', Auth.verifyToken, jsonParser, function (req, res) {
    let tokenUserData = req.body.userData;

    var Orderby;
    var objParam = req.body;
    var search = {};
    search["$and"] = [];

    Orderby = {
        created_at: -1
    };
    User.findOne({
        _id: ObjectId(tokenUserData._id),
    }).then(function (objUserExist) {
        if (objUserExist != null) {
            search["$and"].push({
                _id: ObjectId(tokenUserData._id)
            });
            User.aggregate([
                {
                    $match: search
                },
                { $set: { user_id: { "$toString": tokenUserData._id } } },
                {
                    $lookup: {
                        from: 'tblgalleries',
                        localField: 'user_id',
                        foreignField: 'userid',
                        as: 'tblgalleries'
                    },
                },
                {
                    $lookup: {
                        from: 'tblposts',
                        localField: 'user_id',
                        foreignField: 'userid',
                        as: 'tblpost'
                    },
                },
                {
                    $unwind: {
                        path: '$tblpost',
                        preserveNullAndEmptyArrays: true
                    }
                },
                { $set: { currentPostId: { "$toString": "$tblpost._id" } } },
                {
                    $lookup: {
                        from: "tblpostfiles",
                        let: {
                            post_id: "$currentPostId"
                        },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [{
                                        $eq: ["$postid", "$$post_id"]
                                    }
                                    ]
                                }
                            }
                        }],
                        as: "tblpost.tblpostfiles",
                    },
                },
                {
                    $group: {
                        _id: '$_id',
                        root: { $mergeObjects: '$$ROOT' },
                        tblpost: { $push: '$tblpost' }
                    }
                },
                {
                    $replaceRoot: {
                        newRoot: {
                            $mergeObjects: ['$root', '$$ROOT']
                        }
                    }
                },
                {
                    $project: {
                        root: 0
                    }
                },
                {
                    $sort: Orderby
                }
            ]).then(function (listUsers) {
                console.log("listUsers", listUsers)
                if (listUsers && listUsers.length > 0) {
                    let newUserObj = listUsers[0];
                    delete newUserObj.password;
                    delete newUserObj.accesstoken;
                    delete newUserObj._id;
                    res.json({
                        success: true,
                        message: "Record(s) found.",
                        data: newUserObj
                    })
                } else {
                    res.json({
                        success: false,
                        message: "No Record(s) found."
                    })
                }
            }).catch(function (error) {
                console.log("error", error)
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "User not found!!"
            })
        }
    }).catch((error) => {
        console.error('[' + moment().format('DD/MM/YYYY hh:mm:ss a') + '] ' + error.stack || error.message);
        res.json({
            success: false,
            message: error.message
        });
    })
});

//Update User
router.post("/updateUser", Auth.verifyToken, jsonParser, function (req, res) {
    let objUser = req.body;

    user.updateUser(objUser.userData, objUser, function (response, code) {
        res.status(code).json(response);
    });
});

//Upload Profile Picture
router.post('/uploadImage', Auth.verifyToken, function (req, res) {
    console.log("req.body", req.body)
    let tokenUserData = req.body.userData;
    user.uploadImage(tokenUserData, req, function (response, code) {
        res.status(code).json(response);
    });
});

//Get User by Uniqueid
router.post('/findUserByUniqueId', Auth.verifyToken, function (req, res) {
    var tokenUserData = req.body.userData;
    var objParam = req.body;

    User.aggregate([{
        $match: {
            uniqueid: {
                $regex: ".*" + objParam.search + ".*"
            }
        },
    },
    {
        "$project": {
            "_id": 1,
            "profilepic": 1,
            "fullname": 1,
            "uniqueid": 1,
        }
    }]).then(function (listUsers) {
        if (listUsers) {
            res.json({
                success: true,
                message: "Record(s) found.",
                data: listUsers
            })
        } else {
            res.json({
                success: false,
                message: "No Record(s) found."
            })
        }
    }).catch(function (error) {
        console.log("error", error)
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
})

//Finding All Users By Latitude and Longitute
router.post('/getAllUsersByLatitudeLongitude', async function (req, res) {
    var objParam = req.body;
    if (objParam.latitude != '' && objParam.latitude != null && objParam.latitude != undefined && objParam.longitude != '' && objParam.longitude != null && objParam.longitude != undefined) {
        getAreaUserLocation(objParam, async function (areaUsers) {
            if (areaUsers.length > 0) {
                User.aggregate([
                    {
                        $match: {
                            _id: {
                                $in: areaUsers
                            }
                        }
                    }, {
                        "$project": {
                            "_id": 1,
                            "uniqueid": 1,
                            "fullname": 1,
                            "isprivate": 1,
                            "profilepic": 1,
                            "bio": 1,
                            "followers": 1,
                            "following": 1,
                        }
                    },
                ]).then(function (UserLists) {
                    if (UserLists.length > 0) {
                        res.json({
                            success: true,
                            message: "Users found.",
                            data: UserLists
                        });
                    } else {
                        res.json({
                            success: false,
                            message: "No Users found."
                        })
                    }
                })
            } else {
                res.json({
                    success: false,
                    message: "No Users found."
                });
            }
        }).catch(function (err) {
            res.json({
                success: false,
                message: err.message
            });
        });
    } else {
        res.json({
            success: false,
            message: "Invalid request!"
        });
    }
});

//Measure Lat Long
function measureAreaByUserLatitudeLongitude(lat1, lon1, lat2, lon2) {
    var R = 6378.137;
    var dLat = (lat2 * Math.PI) / 180 - (lat1 * Math.PI) / 180;
    var dLon = (lon2 * Math.PI) / 180 - (lon1 * Math.PI) / 180;
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000;
}

//function for finding all Users Id in area
async function getAreaUserLocation(objParam, callback) {
    var maxRadious = 15 * 1000;
    var areaUsers = [];
    var ratedAreaVendors = [];
    // await setting.findOne({
    //     name: "UserRadius"
    // }).then(function (radious) {
    //     maxRadious = radious.value * 1000;
    // });
    User.find()
        .then(async function (users) {
            console.log("users:", users)
            if (users.length > 0) {
                await users.forEach(async (element) => {
                    const metersFloat = await measureAreaByUserLatitudeLongitude(
                        objParam.latitude,
                        objParam.longitude,
                        element.latitude,
                        element.longitude
                    );
                    const metersInt = parseInt(metersFloat);
                    if (maxRadious > metersInt) {
                        areaUsers.push(ObjectId(element._id));
                    }
                });

                return callback(areaUsers);
            } else {
                return callback({
                    success: false,
                    message: "No Users found.",
                });
            }
        })
        .catch(function (err) {
            console.log("error:", err.message)
            return callback({
                success: false,
                message: err.message,
            });
        });
}

//Get user detail by uniqueid
router.post('/GetProfilebyuserId', Auth.verifyToken, jsonParser, function (req, res) {
    let tokenUserData = req.body.userData;

    var Orderby;
    var objParam = req.body;

    var search = {};
    search["$and"] = [];
    Orderby = {
        created_at: -1
    };
    User.findOne({
        _id: ObjectId(tokenUserData._id),
    }).then(function (objCurrentUserExist) {
        // search['$and'].push({ "whofollow": tokenUserData._id });
        search['$and'].push({ "uniqueid": objParam.uniqueid });
        search['$and'].push({ "tblfollows.whofollow": (tokenUserData._id).toString() });
        User.aggregate([
            { $set: { _id: { "$toString": "$_id" } } },
            {
                $lookup: {
                    from: 'tblfollows',
                    localField: '_id',
                    foreignField: 'whomfollow',
                    as: 'tblfollows'
                },
            },
            {
                $unwind: {
                    path: "$tblfollows",
                    preserveNullAndEmptyArrays: true,
                },
            }, {
                $match: search
            }
        ]).then(function (objUserExist) {
            let newUserObj = objUserExist[0];
            console.log("objUserExist", objUserExist)
            if (newUserObj.tblfollows && (newUserObj.isprivate == 0 || newUserObj.tblfollows == 1)) {
                findGalleryandPostofUser(newUserObj._id, function (galleryPostRes) {
                    // delete newUserObj.password;
                    // delete newUserObj._id;
                    res.json({
                        success: true,
                        data: newUserObj,
                        gallerydata: galleryPostRes.tblgallery,
                        postdata: galleryPostRes.tblpost
                    })
                })
            } else {
                // delete newUserObj.password;
                // delete newUserObj._id;
                res.json({
                    success: true,
                    data: newUserObj,
                    gallerydata: [],
                    postdata: []
                })
            }
        })
    })

});

function findGalleryandPostofUser(userid, callback) {
    Gallery.find({ userid: userid.toString() }).then((responseGallery) => {
        Post.aggregate([
            {
                $match: { "userid": userid }
            },
            { $set: { _id: { "$toString": "$_id" } } },
            {
                $lookup: {
                    from: 'tblpostfiles',
                    localField: '_id',
                    foreignField: 'postid',
                    as: 'tblpostfiles'
                },
            },
            {
                $sort: { created_at: -1 }
            }
        ]).then(function (responsePost) {
            return callback({
                tblgallery: responseGallery,
                tblpost: responsePost
            })

        }).catch(function (error) {
            console.log(new Date());
            console.log(error.message);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })

        // Post.find({ userid: userid.toString() }).then((responsePost) => {
        //     console.log("responseGallery", responseGallery)
        //     console.log("responsePost", responsePost)
        //     return callback({
        //         tblgallery: responseGallery,
        //         tblpost: responsePost
        //     })
        // })
    })
}

module.exports = router;