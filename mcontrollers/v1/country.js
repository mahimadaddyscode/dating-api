var express = require('express');
var router = express.Router();
// var request = require('request')
const State = require('../../models/tblstate');
const Country = require('../../models/tblcountry');
const City = require('../../models/tblcity');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post('/GetAllCountry', function (req, res) {
    Country.find().sort({
        'country': 1
    }).then(function (response) {
        res.json({
            success: true,
            message: "Country found...",
            data: response
        });
    }).catch(function (error) {
        res.json({
            success: false,
            message: "Country not found...",
            data: error
        });
    })
})
router.post('/GetAllState', function (req, res) {
    State.find().sort({
        'state': 1
    }).then(function (response) {
        res.json({
            success: true,
            message: "State found...",
            data: response
        });
    }).catch(function (error) {
        res.json({
            success: false,
            message: "State not found...",
            data: error
        });
    })
});
router.post('/GetAllCity', function (req, res) {
    City.find().sort({
        'city': 1
    }).then(function (response) {
        res.json({
            success: true,
            message: "City found...",
            data: response
        });
    }).catch(function (error) {
        res.json({
            success: false,
            message: "City not found...",
            data: error
        });
    })
});


//get All States By Country
router.post('/GetAllStateByCountryId', jsonParser, function (req, res) {

    var country = req.body.country;
    if (country != null && country != undefined && country != "") {
        State.find({
            country: country
        }).then(function (response) {
            if (response != null && response.length > 0) {
                res.json({
                    success: true,
                    message: "Records found...",
                    data: response
                });
            } else {
                res.json({
                    success: false,
                    message: "Record not found...",
                    data: response
                });
            }
        })
    } else {
        res.json({
            success: false,
            message: "Country is required"
        });
    }
});

//get All Cities By State
router.post('/GetAllCityByStateId', jsonParser, function (req, res) {
    var state = req.body.state;
    if (state != null && state != undefined && state != "") {
        City.find({
            state: state
        }).then(function (response) {
            if (response != null && response.length > 0) {
                res.json({
                    success: true,
                    message: "Record found...",
                    data: response
                });
            } else {
                res.json({
                    success: false,
                    message: "Record not found...",
                    data: response
                });
            }
        })
    } else {
        res.json({
            success: false,
            message: "State is required"
        });
    }
});


router.post('/SaveCountry', jsonParser, function (req, res) {
    var objParam = req.body;
    Country.create(objParam).then(function (resCreate) {
        res.json({
            success: true,
            message: "Country created successfully.",
        })
    }).catch(function (error) {
        console.log(new Date());
        console.log(error.message);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
})

router.post('/SaveState', jsonParser, function (req, res) {
    var objParam = req.body;
    State.create(objParam).then(function (resCreate) {
        res.json({
            success: true,
            message: "State created successfully.",
        })
    }).catch(function (error) {
        console.log(new Date());
        console.log(error.message);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
});


router.post('/SaveCity', jsonParser, function (req, res) {
    var objParam = req.body;
    City.create(objParam).then(function (resCreate) {
        res.json({
            success: true,
            message: "City created successfully.",
        })
    }).catch(function (error) {
        console.log(new Date());
        console.log(error.message);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
})
module.exports = router