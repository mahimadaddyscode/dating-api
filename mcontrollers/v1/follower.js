var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var u = require('underscore');
const mongoose = require('mongoose');
const check = require('../../libs/checkLib');
const Follow = require('../../models/tblfollow');
const ObjectId = require('mongodb').ObjectID;
const User = require('../../models/tbluser');
var Auth = require('../../middleware/auth');


//send follow request
router.post('/SendFollowRequest', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
        if (response) {
            if (response.isprivate == 0) {
                objParam['status'] = 1;
            } else {
                objParam['status'] = 0;
            }
            Follow.findOneAndUpdate({
                whofollow: (objParam.userData._id).toString(),
                whomfollow: (response._id).toString()
            }, objParam, {
                upsert: true, 'new': true
            }).then(function (resFollow) {
                if (resFollow.status == 1) {
                    User.findOneAndUpdate({ _id: ObjectId(objParam.userData._id) }, { $inc: { 'following': 1 } }).then(function (resSFollow) {
                        User.findOneAndUpdate({ _id: ObjectId(response._id) }, { $inc: { 'followers': 1 } }).then(function (resRFollow) {

                        });
                    });
                }
                if (resFollow != null) {
                    res.json({
                        success: true,
                        message: "Follow request send.",
                    })
                } else {
                    res.json({
                        success: false,
                        message: "You are already Following to this user.",
                    })
                }
            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "No User found.",
            })
        }

    }).catch((error) => {
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })

})

//unfollow user
router.post('/UnfollowProfile', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    if (objParam.userData._id != null && objParam.userData._id != undefined && objParam.userData._id != '' && objParam.uniqueid != null && objParam.uniqueid != undefined && objParam.uniqueid != '') {
        User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
            if (response) {
                Follow.findOneAndDelete({
                    whofollow: (objParam.userData._id).toString(),
                    whomfollow: (response._id).toString(),
                    status: 1
                }).then(function (resFollow) {
                    res.json({
                        success: true,
                        message: "Request deleted successfully.",
                    })
                }).catch(function (error) {
                    console.log(new Date());
                    res.json({
                        success: false,
                        message: "Something went wrong.",
                    })
                })
            } else {
                res.json({
                    success: false,
                    message: "No User found.",
                })
            }
        }).catch((error) => {
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })
    } else {
        res.json({
            success: false,
            message: "whofollow and whomfollow both are required.",
        })
    }
})

//Verify reject/accept the follow request
router.post('/VerifyFollowRequest', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    if (objParam.userData._id != null && objParam.userData._id != undefined && objParam.userData._id != '' && objParam.uniqueid != null && objParam.uniqueid != undefined && objParam.uniqueid != '') {
        User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
            if (response) {
                if (objParam.status == -1) {
                    Follow.findOneAndDelete({
                        whofollow: (objParam.userData._id).toString(),
                        whomfollow: (response._id).toString()
                    }).then(data => {
                        if (data) {
                            res.json({
                                success: true,
                                message: "User Verified successfully.",
                            })
                        } else {
                            res.json({
                                success: false,
                                message: "Something went wrong.",
                            })
                        }
                    }).catch(function (error) {
                        console.log(new Date());
                        console.log(error.message);
                        res.json({
                            success: false,
                            message: "Something went wrong.",
                        })
                    })
                } else {
                    console.log("objParam.userData._id", objParam.userData._id)
                    console.log("response._id", response._id)
                    Follow.findOneAndUpdate({
                        whomfollow: (objParam.userData._id).toString(),
                        whofollow: (response._id).toString(),
                        status: 0
                    }, objParam, {
                        new: true
                    }).then(data => {
                        if (data) {
                            User.findOneAndUpdate({ _id: ObjectId(objParam.userData._id) }, { $inc: { 'followers': 1 } }).then(function (resSFollow) {
                                User.findOneAndUpdate({ _id: ObjectId(response._id) }, { $inc: { 'following': 1 } }).then(function (resRFollow) {
                                    res.json({
                                        success: true,
                                        message: "User Verified successfully.",
                                    })
                                });
                            });

                        } else {
                            console.log("herere")
                            res.json({
                                success: false,
                                message: "Something went wrong.",
                            })
                        }
                    }).catch(function (error) {
                        console.log(new Date());
                        console.log(error.message);
                        res.json({
                            success: false,
                            message: "Something went wrong.",
                        })
                    })
                }
            } else {
                res.json({
                    success: false,
                    message: "No User found.",
                })
            }
        })
    } else {
        res.json({
            success: false,
            message: "SProfileid and Rprofileid both are required.",
        })
    }
})

//Get all pending follow request by token
router.post('/GetAllPendingFollowRequest', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    Orderby = {
        created_at: -1
    };
    Follow.aggregate([{
        $match: {
            whomfollow: (objParam.userData._id).toString(),
            status: 0
        }
    },
    { "$addFields": { "whofollow": { "$toObjectId": "$whofollow" } } },
    {
        $lookup: {
            from: 'tblusers',
            localField: 'whofollow',
            foreignField: '_id',
            as: 'tblusers'
        },
    },
    { "$unwind": "$tblusers" }, {
        "$project": {
            "_id": 1,
            "whofollow": 1,
            "whomfollow": 1,
            "status": 1,
            "tblusers.profilepic": 1,
            "tblusers.fullname": 1,
            "tblusers.uniqueid": 1,
        }
    },
    {
        $sort: Orderby
    },
    {
        $limit: objParam.limit
    },
    {
        $skip: objParam.limit * objParam.offset
    },
    ]).then(function (listFollow) {
        res.json({
            success: true,
            message: "Record(s) found.",
            data: listFollow
        })

    }).catch(function (error) {
        console.log(new Date());
        console.log(error.message);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })

});

//Get all followers by uniqueid
router.post('/GetAllFollowersById', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    Orderby = {
        created_at: -1
    };
    User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
        if (response) {
            Follow.aggregate([{
                $match: {
                    whomfollow: (response._id).toString(),
                    status: 1
                }
            },
            { "$addFields": { "whofollow": { "$toObjectId": "$whofollow" } } },
            {
                $lookup: {
                    from: 'tblusers',
                    localField: 'whofollow',
                    foreignField: '_id',
                    as: 'tblusers'
                },
            },
            { "$unwind": "$tblusers" }, {
                "$project": {
                    "_id": 1,
                    "whofollow": 1,
                    "whomfollow": 1,
                    "status": 1,
                    "tblusers.profilepic": 1,
                    "tblusers.fullname": 1,
                    "tblusers.uniqueid": 1,
                }
            },
            {
                $sort: Orderby
            },
            {
                $limit: objParam.limit
            },
            {
                $skip: objParam.limit * objParam.offset
            },
            ]).then(function (listFollow) {
                res.json({
                    success: true,
                    message: "Record(s) found.",
                    data: listFollow
                })

            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })

        } else {
            res.json({
                success: false,
                message: "No User found.",
            })
        }
    })


});

//Get all following by uniqueid
router.post('/GetAllFollowingById', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    Orderby = {
        created_at: -1
    };
    User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
        if (response) {
            Follow.aggregate([{
                $match: {
                    whofollow: (response._id).toString(),
                    status: 1
                }
            },
            { "$addFields": { "whomfollow": { "$toObjectId": "$whomfollow" } } },
            {
                $lookup: {
                    from: 'tblusers',
                    localField: 'whomfollow',
                    foreignField: '_id',
                    as: 'tblusers'
                },
            },
            { "$unwind": "$tblusers" },
            {
                "$project": {
                    "_id": 1,
                    "whofollow": 1,
                    "whomfollow": 1,
                    "status": 1,
                    "tblusers.profilepic": 1,
                    "tblusers.fullname": 1,
                    "tblusers.uniqueid": 1,
                }
            },
            {
                $sort: Orderby
            },
            {
                $limit: objParam.limit
            },
            {
                $skip: objParam.limit * objParam.offset
            },
            ]).then(function (listFollow) {
                res.json({
                    success: true,
                    message: "Record(s) found.",
                    data: listFollow
                })

            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "No User found.",
            })
        }
    });

});

//Get all blocklist by token
router.post('/GetBlockedList', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    console.log("objParam.userData._id", objParam.userData._id)
    if (objParam.userData._id != null && objParam.userData._id != undefined && objParam.userData._id != '') {
        Follow.aggregate([{
            "$match": {
                "whofollow": (objParam.userData._id).toString(),
                status: -1
            }
        },
        { "$addFields": { "whomfollow": { "$toObjectId": "$whomfollow" } } }, {
            $lookup: {
                from: "tblusers",
                localField: 'whomfollow',
                foreignField: '_id',
                as: 'tblusers'
            },
        }, { "$unwind": "$tblusers" }, {
            "$project": {
                "_id": 1,
                "whofollow": 1,
                "whomfollow": 1,
                "status": 1,
                "tblusers.profilepic": 1,
                "tblusers.fullname": 1,
                "tblusers.uniqueid": 1,
            }
        }, {
            $limit: objParam.limit
        }, {
            $skip: objParam.limit * objParam.offset
        }]).then(function (data) {
            res.json({
                success: true,
                message: "Record(s) found.",
                data: data
            })
        }).catch(error => {
            console.log(new Date());
            console.log(error.message);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })
    } else {
        res.json({
            success: false,
            message: "Profileid is required.",
        })
    }
})

//Block user
router.post('/BlockUser', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    User.findOne({ uniqueid: objParam.uniqueid }).then((response) => {
        if (response) {
            objParam['status'] = -1;
            Follow.findOneAndUpdate({
                whofollow: objParam.userData._id,
                whomfollow: response._id
            }, objParam, {
                upsert: true, 'new': true
            }).then(function (resFollow) {
                if (resFollow != null) {
                    res.json({
                        success: true,
                        message: "Blocked user.",
                    })
                } else {
                    res.json({
                        success: false,
                        message: "You are already Blocked this user.",
                    })
                }
            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "No User found.",
            })
        }

    }).catch((error) => {
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })

})

module.exports = router;