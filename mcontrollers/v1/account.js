var express = require('express');
var router = express.Router();
var moment = require('moment');
var common = require('../../common/common');
var user = require('../../common/user');
var smsmail = require('../../common/smsmail');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var TokenKey = process.env.tokenkey;
const User = require('../../models/tbluser');
const UserVerification = require('../../models/tbluserotpverification');
const Role = require('../../models/tblrole');
var Auth = require('../../middleware/auth');
var QRCode = require("qrcode");
var formidable = require('formidable');
var fs = require('fs');
var jwt = require('jsonwebtoken');
const ObjectId = require('mongodb').ObjectID;



//Check Unique ID of User
router.post('/CheckUniqueidExist', jsonParser, function (req, res) {
    var objParam = req.body;
    User.findOne({ uniqueid: (objParam.uniqueid).toLowerCase() }).then(function (resExist) {
        if (resExist != null) {
            res.json({
                success: true,
                message: "User Found"
            });

        } else {
            res.json({
                success: false,
                message: "User Not Found"
            });
        }
    })
});

//send OTP to phone number when login
router.post("/SendLoginOTP", jsonParser, function (req, res) {
    var objParam = req.body;
    // account.SendLoginOTP(objUser, function (respas) {
    //     res.json(respas);
    // });
    var today = common.CurrentTime();
    var objPassword = new Object();
    objPassword.phone = "91" + objParam.phone
    objPassword.otp = common.GenerateOTP();
    objPassword.date = common.CurrentTime();
    objPassword.expirydate = parseInt(objPassword.date) + 30 * 60000;
    UserVerification.findOneAndUpdate({
        phone: "91" + objPassword.phone
    }, {
        $setOnInsert: objPassword
    }, {
        returnOriginal: false,
        upsert: true,
    }).then(function (resVerify) {
        if (!resVerify) {
            smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                res.json(resSMS);
            }
            );
        }
        else {
            if (today < resVerify.expirydate) {
                objPassword = resVerify;
                smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                    res.json(resSMS);
                }
                );
            }
            else {
                objPassword._id = ObjectId(resVerify._id);
                UserVerification.updateOne({
                    phone: "91" + objPassword.phone
                }, {
                    $set: {
                        otp: objPassword.otp,
                        expirydate: objPassword.expirydate,
                    },
                }).then(function (resUpdate) {
                    smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                        res.json(resSMS);
                    });
                });
            }
        }
    })
});

//verify OTP to phone number when login or register
router.post("/CheckLoginOTP", jsonParser, function (req, res) {
    var objParam = req.body;
    console.log("objParam", objParam)
    UserVerification.aggregate([{
        $match: {
            otp: objParam.OTP,
            phone: "91" + objParam.phone
        },
    },
    ]).then(function (resExist) {
        if (resExist.length > 0 && resExist != null) {
            var today = common.CurrentTime();
            var expiryDate = resExist[0].expirydate;
            if (today < expiryDate) {
                console.log("heree")
                User.aggregate([{
                    $match: {
                        phone: "91" + objParam.phone
                    }
                },
                { $set: { roleid: { $toObjectId: "$roleid" } } },
                {
                    $lookup: {
                        from: "tblroles",
                        localField: "roleid",
                        foreignField: "_id",
                        as: "tblrole",
                    },
                },
                {
                    $unwind: {
                        path: "$tblrole",
                        preserveNullAndEmptyArrays: true,
                    },
                },
                ]).then(function (resUser) {
                    console.log("resUser", resUser)
                    if (resUser.length > 0 && resUser != null) {
                        var user = {
                            _id: resUser[0]._id,
                            phone: resUser[0].phone,
                            email: resUser[0].email,
                            Role: resUser[0].tblrole.rolename,
                            RoleType: resUser[0].tblrole.type,
                        };
                        jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                            res.json({
                                success: true,
                                userexists: true,
                                token: token,
                                UserId: resUser[0]._id,
                                Uniqueid: resUser[0].uniqueid,
                                profileid: resUser[0].profileid,
                                FullName: resUser[0].fullname,
                                Email: resUser[0].email,
                                Phone: resUser[0].phone,
                                UserImage: resUser[0].image,
                                UserRole: resUser[0].tblrole.rolename,
                                UserRoleType: resUser[0].tblrole.type,
                                message: "Login Successfully !!",
                            });
                            User.findOneAndUpdate({
                                phone: resUser[0].phone,
                                uniqueid: resUser[0].uniqueid,
                                $set: {
                                    accesstoken: token
                                }
                            }).then(function (response) {

                            })
                        });
                        UserVerification.remove({
                            phone: resUser[0].phone,
                        });
                    }
                    else {
                        Role.findOne({
                            type: "User"
                        }).then(function (resRole) {
                            if (objParam.phone != null && objParam.phone != undefined && objParam.phone != '') {
                                objParam.phone = "91" + objParam.phone
                            }
                            res.json({
                                success: true,
                                userexists: false,
                                Phone: objParam.phone
                            });
                            UserVerification.remove({
                                phone: objParam.phone,
                            });
                        })
                    }
                })
            } else {
                UserVerification.remove({
                    phone: objParam.phone,
                });
                res.json({
                    success: false,
                    message: "OTP is expired!!",
                });
            }
        }
        else {
            res.json({
                success: false,
                message: "Invalid OTP or Phone No !!",
            });
        }
    });
});

// Register new User
router.post('/Register', jsonParser, function (req, res) {
    var objParam = req.body;
    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../../mediaupload/profilepic';

    var objFile;

    form.parse(req, function (err, fields, files) {
        objParam = JSON.parse(fields.objUser);
    });

    form.on('fileBegin', function (name, file) {
        var ext = file.name.substring(file.name.indexOf('.'), file.name.length);
        common.GetUserNameFromDateWithCallback(function (resName) {
            var NewName = resName + 2;
            NewName = "B" + NewName + ext;
            file.path = form.uploadDir + "/" + NewName;
            objFile = NewName;
        });
    });

    form.on('end', function () {
        if (objParam.fullname && objParam.fullname != '' && objParam.uniqueid && objParam.uniqueid != '') {
            User.findOne({ $or: [{ uniqueid: (objParam.uniqueid).toLowerCase() }, { phone: objParam.phone }] }).then(function (resUser) {
                if (resUser != null) {
                    if (objParam.phone == resUser.phone) {
                        res.json({
                            success: false,
                            message: "Phone number is already used by another User",
                        });
                    } else {
                        res.json({
                            success: false,
                            message: "Uniqueid is already used by another User",
                        });
                    }
                } else {
                    objParam.createddate = common.CurrentTime();
                    common.GetUserNameFromDateWithCallback(function (resName) {
                        var NewName = resName + 2 + ".svg";
                        var QRdata = {
                            fullname: objParam.fullname,
                            phone: objParam.phone,
                            uniqueid: (objParam.uniqueid).toLowerCase(),
                        };
                        jwt.sign({ QRdata }, TokenKey, (err, token) => {
                            // var token = jwt.encode(QRdata, TokenKey);

                            QRCode.toFile(
                                "./mediaupload/qrcode/" + NewName,
                                token, {
                                type: "svg",
                            },
                                function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    objParam.qrimage = NewName;
                                    if (objFile && objFile != undefined) {
                                        objParam.profilepic = objFile;
                                    }
                                    Role.findOne({ type: 'User' }).then(function (resRole) {
                                        objParam.roleid = resRole._id;
                                        objParam.uniqueid = (objParam.uniqueid).toLowerCase();
                                        console.log("resRole", resRole)
                                        console.log("resUser", resUser)
                                        console.log("objParam", objParam)
                                        User.create(objParam).then(function (resUser1) {
                                            console.log("resUser1", resUser1)
                                            var user = {
                                                _id: resUser1._id,
                                                phone: resUser1.phone,
                                                email: resUser1.email,
                                                Role: resRole.rolename,
                                                RoleType: resRole.type,
                                            };
                                            console.log("user", user)
                                            jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, accesstoken) => {
                                                User.findOneAndUpdate({
                                                    _id: ObjectId(resUser1._id),
                                                },
                                                    { accesstoken: accesstoken },
                                                    {
                                                        new: true
                                                    }).then(function (responseUs) {
                                                        res.json({
                                                            success: true,
                                                            token: responseUs.accesstoken,
                                                            UserId: responseUs._id,
                                                            Uniqueid: responseUs.uniqueid,
                                                            profileid: responseUs.profileid,
                                                            FullName: responseUs.fullname,
                                                            Email: responseUs.email,
                                                            Phone: responseUs.phone,
                                                            UserImage: responseUs.image,
                                                            UserRole: resRole.rolename,
                                                            UserRoleType: resRole.type,
                                                            message: "Login Successfully !!",
                                                        });
                                                    })

                                            })

                                        }).catch((err) => {
                                            console.log("err", err)
                                            res.json({
                                                success: false,
                                                message: "Something went wrong..."
                                            });
                                        })
                                    }).catch((err) => {
                                        res.json({
                                            success: false,
                                            message: "Something went wrong..."
                                        });
                                    })
                                })

                        });
                    });
                }
            }).catch((err) => {
                res.json({
                    success: false,
                    message: "Something went wrong..."
                });
            })
        } else {
            res.json({
                success: false,
                message: "Required Parameter password and fullname is missing",
            });
        }
    })
});

//Login with gmail and facebook
router.post("/LoginWithGmailorFB", jsonParser, function (req, res) {
    var objParam = req.body;

    User.aggregate([{
        $match: {
            $or: [
                {
                    profileid: objParam.profileid
                },
                {
                    email: objParam.email.trim()
                },
            ],
        }
    },
    { $set: { roleid: { $toObjectId: "$roleid" } } },
    {
        $lookup: {
            from: "tblroles",
            localField: "roleid",
            foreignField: "_id",
            as: "tblrole",
        },
    },
    {
        $unwind: {
            path: "$tblrole",
            preserveNullAndEmptyArrays: true,
        },
    },
    ]).then(function (resUser) {
        if (resUser.length > 0 && resUser != null) {
            var user = {
                _id: resUser[0]._id,
                phone: resUser[0].phone,
                email: resUser[0].email,
                profileid: resUser[0].profileid,
                Role: resUser[0].tblrole.rolename,
                RoleType: resUser[0].tblrole.roletype,
            };
            jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                res.json({
                    success: true,
                    userexists: true,
                    token: token,
                    UserId: resUser[0]._id,
                    FullName: resUser[0].fullname,
                    Email: resUser[0].email,
                    Phone: resUser[0].phone,
                    UserImage: resUser[0].image,
                    UserRole: resUser[0].tblrole.rolename,
                    UserRoleType: resUser[0].tblrole.roletype,
                    message: "Login Successfully !!",
                });
                User.findOneAndUpdate({
                    profileid: resUser[0].profileid,
                    uniqueid: resUser[0].uniqueid,
                    $set: {
                        accesstoken: token
                    }
                }).then(function (response) {

                })
            });
            UserVerification.remove({
                phone: resUser[0].phone,
            });
        }
        else {
            Role.findOne({
                type: "User"
            }).then(function (resRole) {
                objParam.roleid = resRole._id;
                objParam.createddate = common.CurrentTime();
                if (objParam.phone != null && objParam.phone != undefined && objParam.phone != '') {
                    objParam.phone = "91" + objParam.phone
                }
                User.create(objParam).then(function (resUserCreated) {
                    if (resUserCreated) {
                        var user = {
                            _id: resUserCreated._id,
                            phone: resUserCreated.phone,
                            email: resUserCreated.email,
                            Role: resRole.rolename,
                            RoleType: resRole.type,
                        };
                        jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                            res.json({
                                success: true,
                                userexists: false,
                                token: token,
                                UserId: resUserCreated._id,
                                FullName: resUserCreated.fullname,
                                Email: resUserCreated.email,
                                Phone: resUserCreated.phone,
                                UserImage: resUserCreated.image,
                                UserRole: resRole.rolename,
                                UserRoleType: resRole.roletype,
                                message: "Login Successfully !!",
                            });
                        });
                        UserVerification.remove({
                            phone: objParam.phone,
                        });
                    }
                    else {
                        res.json({
                            success: false,
                            message: "Something went wrong. Please try again later !!",
                        });
                    }
                })
            })
        }
    })

});

module.exports = router;