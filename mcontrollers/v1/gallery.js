var express = require('express');
var router = express.Router();
var moment = require('moment');
var user = require('../../common/user');
var common = require('../../common/common');
var Auth = require('../../middleware/auth');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
const User = require('../../models/tbluser');
const Gallery = require('../../models/tblgallery');
const ObjectId = require('mongodb').ObjectID;
var formidable = require('formidable');
var fs = require('fs');

//upload gallery image
router.post('/uploadImagetoGallery', Auth.verifyToken, jsonParser, function (req, res) {
    let tokenUserData = req.body.userData;

    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../mediaupload/gallery';
    var FileName = [];

    form.parse(req, function (err, fields, files) {
        //you can get fields here
    });

    form.on('fileBegin', function (name, file) {
        var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
        common.GetUserNameFromDateWithCallback(function (resName) {
            var NewName = resName + 2;
            file.path = form.uploadDir + "/" + NewName + ext;
            FileName.push(NewName + ext);
        });
    });

    form.on("end", function () {
        var imgArr = [];
        if (FileName) {
            for (let i = 0; i < FileName.length; i++) {

                let imgobj = {
                    userid: tokenUserData._id,
                    file: FileName[i],
                    type: FileName[i].substring(FileName[i].lastIndexOf('.'), FileName[i].length)
                }
                imgArr.push(imgobj);
            }
        }
        Gallery.create(imgArr).then(function (response1) {
            res.json({
                success: true,
                message: "Saved Successfully!!",
                imgRes: response1,
            });
        }).catch(function (err) {
            console.error('[' + moment().format('DD/MM/YYYY hh:mm:ss a') + '] ' + err.stack || err.message);
            res.json({
                success: false,
                message: err.message
            });
        });
    });

});

//get all gallery image by token
router.post('/GetGalleryDataByUserID', Auth.verifyToken, jsonParser, function (req, res) {
    let tokenUserData = req.body.userData;

    var Orderby;
    var objParam = req.body;
    var search = {};
    search["$and"] = [];

    Orderby = {
        created_at: -1
    };

    User.findOne({
        _id: ObjectId(tokenUserData._id),
    }).then(function (objUserExist) {
        if (objUserExist != null) {
            search["$and"].push({
                userid: objUserExist._id.toString()
            });
            Gallery.aggregate([
                {
                    $match: search
                },
                // { $set: { userid: { $toObjectId: "$userid" } } },
                // {
                //     $lookup: {
                //         from: 'tblusers',
                //         localField: 'userid',
                //         foreignField: '_id',
                //         as: 'tblusers'
                //     },
                // },
                // {
                //     $unwind: {
                //       path: "$tblusers",
                //       preserveNullAndEmptyArrays: true,
                //     },
                // },
                {
                    $sort: Orderby
                },
                {
                    $limit: objParam.limit
                },
                {
                    $skip: objParam.limit * objParam.offset
                }
            ]).then(function (listGallery) {
                if (listGallery) {
                    res.json({
                        success: true,
                        message: "Record(s) found.",
                        data: listGallery
                    })
                } else {
                    res.json({
                        success: false,
                        message: "No Record(s) found."
                    })
                }
            }).catch(function (error) {
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "User not found!!"
            })
        }
    }).catch((error) => {
        console.error('[' + moment().format('DD/MM/YYYY hh:mm:ss a') + '] ' + error.stack || error.message);
        res.json({
            success: false,
            message: error.message
        });
    })
});

module.exports = router;