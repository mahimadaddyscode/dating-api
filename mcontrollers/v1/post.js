var express = require('express');
var router = express.Router();
var moment = require('moment');
var common = require('../../common/common');
var Auth = require('../../middleware/auth');
var user = require('../../common/user');
var smsmail = require('../../common/smsmail');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var TokenKey = process.env.tokenkey;
const User = require('../../models/tbluser');
const Post = require('../../models/tblpost');
const PostFiles = require('../../models/tblpostfiles');
const PostLike = require('../../models/tblpostlike');
const PostComment = require('../../models/tblpostcomment');
const Follow = require('../../models/tblfollow');
var QRCode = require("qrcode");
var formidable = require('formidable');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var extract = require('extract-zip')
var fs = require('fs')
var unzipper = require('unzipper');
const ObjectId = require('mongodb').ObjectID;
var u = require('underscore');


//Get All post by uniqueid
router.post('/GetAllPostByUserid', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    var search = {};
    search["$and"] = [];

    Orderby = {
        created_at: -1
    };
    User.findOne({ uniqueid: objParam.uniqueid }).then((RespUser) => {
        if (RespUser) {
            search["$and"].push({
                userid: (RespUser._id).toString()
            });
            Post.aggregate([
                {
                    $match: search
                },
                // { $set: { userid: { $toObjectId: "$userid" } } },
                // {
                //     $lookup: {
                //         from: 'tblusers',
                //         localField: 'userid',
                //         foreignField: '_id',
                //         as: 'tblusers'
                //     },
                // },
                { $set: { _id: { "$toString": "$_id" } } },
                {
                    $lookup: {
                        from: 'tblpostfiles',
                        localField: '_id',
                        foreignField: 'postid',
                        as: 'tblpostfiles'
                    },
                },
                {
                    $sort: Orderby
                },
                {
                    $limit: objParam.limit
                },
                {
                    $skip: objParam.limit * objParam.offset
                }
            ]).then(function (listStory) {
                res.json({
                    success: true,
                    message: "Record(s) found.",
                    data: listStory
                })

            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "User not found!!"
            })
        }
    }).catch((error) => {
        console.log(new Date());
        console.log(error);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
});

//Trying to make create Post via zip (Not working currently)
router.post('/CreatePostZip', jsonParser, function (req, res) {
    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../../mediaupload/post/';
    var FileName = [];
    var count = 0;
    form.parse(req, function (err, fields, files) {

    });
    form.on('fileBegin', function (name, file) {
        // var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
        console.log("req.file", file)
        // extract(file.path, {
        //     dir: __dirname + '/../../mediaupload/post/'
        // }, function (err, response) {
        //     // handle err
        //     console.log("err", err)
        //     var zip = new unzip('/../../mediaupload/post/zip.zip');
        //     console.log("zip", zip)
        //     var zipEntries = zip.getEntries();
        //     console.log("zipEntries", zipEntries)
        //     zipEntries.forEach(function (zipEntry) {
        //         count++;
        //     })

        // })

        fs.createReadStream('../zip.zip')
            .pipe(unzipper.Parse())
            .on('entry', function (entry) {
                console.log("entry", entry);
                const fileName = entry.path;
                const type = entry.type; // 'Directory' or 'File'
                const size = entry.vars.uncompressedSize; // There is also compressedSize;
                console.log("fileName", fileName);
                console.log("type", type);
                console.log("size", size);
                if (fileName === "this IS the file I'm looking for") {
                    entry.pipe(fs.createWriteStream('output/path'));
                } else {
                    entry.autodrain();
                }
            });
        // common.GetUserNameFromDateWithCallback(function (resName) {
        //     var NewName = resName + ext;
        //     file.path = form.uploadDir + "/" + NewName;
        //     var arr = name.split(',');
        //     FileName.push({
        //         profileid: arr[0],
        //         storytype: arr[1],
        //         storyid: arr[2] != null && arr[2] != undefined && arr[2] != '' ? arr[2] : null,
        //         file: NewName,
        //         starttime: common.CurrentTime()
        //     })
        // });
    });
    form.on('end', function () {
        console.log("count", count)
        // var objStory = FileName[0];
        // if (objStory.storyid == null) {
        //     Post.find().sort({
        //         storyid: -1
        //     }).limit(1).then(function (lststory) {
        //         if (lststory == null || lststory.length == 0) {
        //             objStory.storyid = parseInt(lststory[0].storyid)
        //         } else {
        //             objStory.storyid = 1
        //         }
        //         Post.create(objStory).then(function (resUpdate) {
        //             res.json({
        //                 success: true,
        //                 message: "Post Uploaded successfully.",
        //             });
        //         }).catch(function (error) {
        //             console.log(new Date());
        //             console.log(error.message);
        //             res.json({
        //                 success: false,
        //                 message: "Something went wrong.",
        //                 data: {}
        //             })
        //         })
        //     })
        // } else {
        //     Post.findOneAndUpdate({
        //         storyid: objStory.storyid
        //     }, objStory, {
        //         upsert: true
        //     }).then(function (res) {
        //         res.json({
        //             success: true,
        //             message: "Post Updated successfully.",
        //         });
        //     }).catch(function (error) {
        //         console.log(new Date());
        //         console.log(error.message);
        //         res.json({
        //             success: false,
        //             message: "Something went wrong.",
        //             data: {}
        //         })
        //     })
        // }
    });
});

//Create Post with multiple images/videos
router.post('/CreatePost', Auth.verifyToken, jsonParser, function (req, res) {
    let tokenUserData = req.body.userData;
    var objParam = req.body;
    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../../mediaupload/post/';
    var FileName = [];
    var lstRequest = [];
    form.parse(req, function (err, fields, files) {
        lstRequest = JSON.parse(fields.objRequest);
    });
    form.on('fileBegin', function (name, file) {
        var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
        common.GetUserNameFromDateWithCallback(function (resName) {
            var NewName = resName + ext;
            file.path = form.uploadDir + "/" + NewName;
            var arr = name.split(',');
            FileName.push({
                type: arr[0],
                file: NewName
            })
        });
    });

    form.on('end', function () {
        if (FileName.length > 0) {
            var PostImageArray = [];
            lstRequest.userid = tokenUserData._id;
            Post.create(lstRequest).then(function (resPost) {
                MakePost(0);
                function MakePost(x) {
                    if (x < FileName.length) {
                        var postImageObj = new Object();
                        postImageObj.userid = tokenUserData._id;
                        postImageObj.postid = resPost._id;
                        postImageObj.type = FileName[x].type;
                        postImageObj.file = FileName[x].file;
                        PostImageArray.push(postImageObj);
                        MakePost(x + 1);
                    } else {
                        PostFiles.insertMany(PostImageArray).then(function (resPostFiles) {
                            res.json({
                                success: true,
                                message: "Post Uploaded successfully.",
                            });
                        })
                    }
                }
            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                    data: {}
                })
            })
        } else {
            Post.findOneAndUpdate({
                _id: lstRequest._id
            }, lstRequest, {
                upsert: true
            }).then(function (resp) {
                res.json({
                    success: true,
                    message: "Post Updated successfully.",
                });
            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                    data: {}
                })
            })
        }
        var objStory = FileName[0];
    });
});

//Delete post by post id
router.post('/deletePost', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    let tokenUserData = req.body.userData;
    if (objParam._id != null && objParam._id != undefined && objParam._id != '' && objParam._id != null) {
        Post.deleteOne({
            _id: ObjectId(objParam._id),
            userid: (tokenUserData._id).toString()
        }).then(function (resFollow) {
            PostFiles.deleteMany({
                postid: objParam._id
            }).then(function (resFollow) {
                res.json({
                    success: true,
                    message: "Request deleted successfully.",
                })
            })

        }).catch(function (error) {
            console.log(new Date());
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })
    } else {
        res.json({
            success: false,
            message: "_id are required.",
        })
    }
})

//like post by post id
router.post('/LikePost', Auth.verifyToken, jsonParser, function (req, res) {
    var listLikes = req.body;
    let tokenUserData = req.body.userData;
    listLikes = listLikes.map(function (el) {
        var o = Object.assign({}, el);
        o.userid = tokenUserData._id;
        return o;
    })
    PostLike.insertMany(listLikes).then(function (resPost) {
        let finalObject;
        resPost.filter((x) => {
            finalObject = x.toObject();
            var objMsg = new Object();
            objMsg.title = "Post liked.";
            objMsg.message = " liked your post.";
            objMsg.type = "PostLiked....";
            sendNotification(objMsg);

            function sendNotification(objMsg) {
                User.findOne({
                    _id: finalObject.userid
                }).then(function (objProfile) {
                    Post.findOne({
                        _id: finalObject.postid
                    }).then(function (objStory) {
                        objMsg.message = objProfile.fullname + objMsg.message;
                        // common.SendAndroidNotification(objMsg, finalObject.userid, function (sendNotification) { });
                    })
                });
            }
            delete finalObject.__v;
            delete finalObject._id;
            delete finalObject.created_at;
            delete finalObject.last_updated;
            delete finalObject.postid;
            delete finalObject.userid;
        })
        var postIDs = u.pluck(resPost, 'postid');
        PostLike.aggregate([{
            "$match": {
                "postid": {
                    "$in": postIDs
                }
            }
        }, { $set: { postid: { $toObjectId: "$postid" } } }, {
            $lookup: {
                from: 'tblposts',
                localField: 'postid',
                foreignField: '_id',
                as: 'tblposts'
            },
        }, {
            $group: {
                "_id": "$postid",
                "like": {
                    "$sum": "$like"
                }
            }
        }]).then(function (listStory) {
            // var postIDs1 = u.pluck(listStory, '_id');
            // console.log("postIDs1", postIDs1)
            const bulkOps = listStory.map(update => ({
                updateOne: {
                    filter: { _id: ObjectId(update._id) },
                    update: { $set: { like: update.like } },
                    upsert: true
                }
            }));
            Post.bulkWrite(bulkOps).then(function (resp) {
                res.json({
                    success: true,
                    message: "Like updated successfully"
                })
            }).catch(function (error) {
                console.log(new Date());
                console.log(error);
                res.json({
                    success: false,
                    message: "Record(s) not found.",
                    data: {}
                })
            })
        }).catch(function (error) {
            console.log(new Date());
            console.log(error);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })
    }).catch(function (error) {
        console.log(new Date());
        console.log(error);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
});

//Get post by post id
router.post('/GetPostById', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;

    if (objParam._id != null && objParam._id != undefined && objParam._id != '' && objParam._id != null) {

        Post.aggregate([
            {
                $match: {
                    _id: ObjectId(objParam._id)
                }
            },
            { $set: { _id: { "$toString": "$_id" } } },
            {
                $lookup: {
                    from: 'tblpostfiles',
                    localField: '_id',
                    foreignField: 'postid',
                    as: 'tblpostfiles'
                },
            }
        ]).then(function (listStory) {
            res.json({
                success: true,
                message: "Record(s) found.",
                data: listStory
            })

        }).catch(function (error) {
            console.log(new Date());
            console.log(error.message);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })

    } else {
        res.json({
            success: false,
            message: "_id are required.",
        })
    }
});

//Get User list who liked the post by post id
router.post('/GetAllUserLikedById', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    Orderby = {
        created_at: -1
    };
    if (objParam._id != null && objParam._id != undefined && objParam._id != '' && objParam._id != null) {

        PostLike.aggregate([{
            $match: {
                postid: objParam._id
            }
        },
        { "$addFields": { "userid": { "$toObjectId": "$userid" } } },
        {
            $lookup: {
                from: 'tblusers',
                localField: 'userid',
                foreignField: '_id',
                as: 'tblusers'
            },
        },
        { "$unwind": "$tblusers" },
        {
            "$project": {
                "_id": 1,
                "postid": 1,
                "tblusers.profilepic": 1,
                "tblusers.fullname": 1,
                "tblusers.uniqueid": 1,
            }
        },
        {
            $sort: Orderby
        },
        {
            $limit: objParam.limit
        },
        {
            $skip: objParam.limit * objParam.offset
        },
        ]).then(function (listFollow) {
            res.json({
                success: true,
                message: "Record(s) found.",
                data: listFollow
            })

        }).catch(function (error) {
            console.log(new Date());
            console.log(error.message);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })

    } else {
        res.json({
            success: false,
            message: "_id are required.",
        })
    }
});

//Comment on post by post id
router.post('/CommentPost', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    let tokenUserData = req.body.userData;
    objParam.userid = tokenUserData._id;
    PostComment.create(objParam).then(function (resCreate) {
        res.json({
            success: true,
            message: "Post Comment send successfully.",
        })
    }).catch(function (error) {
        console.log(new Date());
        console.log(error.message);
        res.json({
            success: false,
            message: "Something went wrong.",
        })
    })
});

//Delete Comment on post by comment id
router.post('/deleteComment', Auth.verifyToken, jsonParser, function (req, res) {
    var objParam = req.body;
    let tokenUserData = req.body.userData;
    if (objParam._id != null && objParam._id != undefined && objParam._id != '' && objParam._id != null) {
        PostComment.findOne({
            _id: ObjectId(objParam._id)
        }).then(function (resPostComment) {
            Post.findOne({
                _id: ObjectId(resPostComment.postid)
            }).then(function (resPost) {
                User.findOne({
                    _id: ObjectId(resPost.userid)
                }).then(function (resUser) {
                    if (tokenUserData._id == resUser._id || tokenUserData._id == resPostComment.userid) {
                        PostComment.deleteMany({
                            $or: [{
                                _id: ObjectId(objParam._id)
                            }, {
                                referenceid: objParam._id
                            }]

                        }).then(function (resFollow) {
                            res.json({
                                success: true,
                                message: "Comment deleted successfully.",
                            })
                        }).catch(function (error) {
                            console.log(error);
                            console.log(new Date());
                            res.json({
                                success: false,
                                message: "Something went wrong.",
                            })
                        })
                    } else {
                        res.json({
                            success: false,
                            message: "You cannot delete the comment",
                        })
                    }
                }).catch(function (error) {
                    console.log(error);
                    console.log(new Date());
                    res.json({
                        success: false,
                        message: "Something went wrong.",
                    })
                })

            }).catch(function (error) {
                console.log(error);
                console.log(new Date());
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        }).catch(function (error) {
            console.log(error);
            console.log(new Date());
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })
    } else {
        res.json({
            success: false,
            message: "_id are required.",
        })
    }
});

//Get All Comments and its reference Comments
router.post('/getAllCommentbyid', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    Orderby = {
        created_at: -1
    };
    if (objParam._id != null && objParam._id != undefined && objParam._id != '' && objParam._id != null) {

        PostComment.aggregate([{
            $match: {
                postid: objParam._id,
                referenceid: ''
            }
        },
        {
            $lookup: {
                from: "tblpostcomments",
                let: {
                    ref_id: { $toString: "$_id" }
                },
                pipeline: [{
                    $match: {
                        $expr: {
                            $and: [{
                                $eq: ["$referenceid", "$$ref_id"]
                            }
                            ]
                        }
                    }
                }],
                as: "tblpostcomments",
            },
        },
        { $set: { userid: { "$toObjectId": "$userid" } } },
        {
            $lookup: {
                from: 'tblusers',
                localField: 'userid',
                foreignField: '_id',
                as: 'tblusers'
            },
        },
        { "$unwind": "$tblusers" },
        {
            "$project": {
                "_id": 1,
                "postid": 1,
                "userid": 1,
                "created_at": 1,
                "comment": 1,
                "referenceid": 1,
                "tblpostcomments.comment": 1,
                "tblpostcomments.referenceid": 1,
                "tblpostcomments.postid": 1,
                "tblpostcomments.userid": 1,
                "tblpostcomments.created_at": 1,
                "tblusers.profilepic": 1,
                "tblusers.fullname": 1,
                "tblusers.uniqueid": 1,
            }
        },
        {
            $sort: Orderby
        },
        {
            $limit: objParam.limit
        },
        {
            $skip: objParam.limit * objParam.offset
        },
        ]).then(function (listFollow) {
            res.json({
                success: true,
                message: "Record(s) found.",
                data: listFollow
            })

        }).catch(function (error) {
            console.log(new Date());
            console.log(error.message);
            res.json({
                success: false,
                message: "Something went wrong.",
            })
        })

    } else {
        res.json({
            success: false,
            message: "_id are required.",
        })
    }
});

//Get All post by uniqueid
router.post('/GetAllPostofFollowingUsers', Auth.verifyToken, jsonParser, function (req, res) {
    var Orderby;
    var objParam = req.body;
    let tokenUserData = req.body.userData;
    var search = {};
    search["$and"] = [];

    Orderby = {
        created_at: -1
    };
    getAllUsersList(tokenUserData._id, function (reponseListId) {
        console.log("reponseListId", reponseListId)
        if (reponseListId.data.length > 0) {
            Post.aggregate([
                {
                    $match: {
                        userid: {
                            $in: reponseListId.data
                        }
                    }
                },
                // { $set: { userid: { $toObjectId: "$userid" } } },
                // {
                //     $lookup: {
                //         from: 'tblusers',
                //         localField: 'userid',
                //         foreignField: '_id',
                //         as: 'tblusers'
                //     },
                // },
                { $set: { _id: { "$toString": "$_id" } } },
                {
                    $lookup: {
                        from: 'tblpostfiles',
                        localField: '_id',
                        foreignField: 'postid',
                        as: 'tblpostfiles'
                    },
                },
                {
                    $sort: Orderby
                },
                {
                    $limit: objParam.limit
                },
                {
                    $skip: objParam.limit * objParam.offset
                }
            ]).then(function (listStory) {
                console.log("listStory", listStory)
                res.json({
                    success: true,
                    message: "Record(s) found.",
                    data: listStory
                })

            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                res.json({
                    success: false,
                    message: "Something went wrong.",
                })
            })
        } else {
            res.json({
                success: false,
                message: "No Following.",
            })
        }
    })
});


function getAllUsersList(userid, callback) {
    User.findOne({ _id: ObjectId(userid) }).then((response) => {
        if (response) {
            Follow.aggregate([{
                $match: {
                    whofollow: (response._id).toString(),
                    status: 1
                }
            },
            // { "$addFields": { "whomfollow": { "$toObjectId": "$whomfollow" } } },
            // {
            //     $lookup: {
            //         from: 'tblusers',
            //         localField: 'whomfollow',
            //         foreignField: '_id',
            //         as: 'tblusers'
            //     },
            // },
            // { "$unwind": "$tblusers" },
            {
                "$project": {
                    "_id": 0,
                    // "tblusers._id": 1,
                    "status": 1,
                    "whofollow": 1,
                    "whomfollow": 1,
                    // "tblusers.profilepic": 1,
                    // "tblusers.fullname": 1,
                    // "tblusers.uniqueid": 1,
                }
            }
            ]).then(function (listFollow) {
                console.log("listFollow", listFollow)
                var userIDs = u.pluck(listFollow, 'whomfollow');
                return callback({
                    success: true,
                    message: "Record(s) found.",
                    data: userIDs
                });

            }).catch(function (error) {
                console.log(new Date());
                console.log(error.message);
                return callback({
                    success: false,
                    message: err.message,
                });
            })
        } else {
            return callback({
                success: false,
                message: "No User Found",
            });
        }
    });
}

module.exports = router;