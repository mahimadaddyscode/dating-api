"use strict";
var jwt = require('jsonwebtoken');
var TokenKey = process.env.tokenkey;
var moment = require("moment");
var u = require("underscore");
const User = require('../models/tbluser');

const ObjectId = require('mongodb').ObjectID;

// var verifyToken = function (req, res, next) {
//     const bearerHeader = req.headers['authorization'];
//     if (typeof bearerHeader !== 'undefined') {
//         const bearer = bearerHeader.split(' ');
//         const bearerToken = bearer[1];
//         req.token = bearerToken;
//         jwt.verify(req.token, TokenKey, (err, authData) => {
//             if (err) {
//                 res.sendStatus(403);
//             } else {
//                 req.authData = authData;
//                 next();
//             }
//         });
//     } else {
//         res.sendStatus(403);
//     }
// };

var verifyToken = function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        jwt.verify(req.token, TokenKey, (err, authData) => {
            // console.log("authData", authData)
            if (err) {
                res.sendStatus(403);
            } else {
                User.findOne({ _id: ObjectId(authData.user._id) }).then(function (userData) {
                    if (userData.accesstoken == req.token) {
                        req.body.userData = userData;
                        next();
                    } else {
                        res.send(401).json({
                            msg: "User is not authenticated."
                        });
                    }
                }).catch(function (err) {
                    res.send(500).json({
                        msg: err.message
                    });
                });
            }
        });
    } else {
        res.sendStatus(403);
    }
};
module.exports = {
    verifyToken: verifyToken
}