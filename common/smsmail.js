var express = require("express");
var common = require("./common");
const SMSTemplate = require("../models/tblsmstemplate");



SendOTPForVerification = function (objParam, objPassword, flgSendSms, callback) {
    objPassword.phone = objPassword.phone.split('').splice(2, 10).join('');
    SMSTemplate.findOne({
        type: "OTP Verification"
    }).then(function (resSMS) {
        if (resSMS != null) {
            var body = resSMS.body.replace(/{OTP}/g, objPassword.otp);
            var obj = new Object();
            obj.number = objParam.phone;
            obj.body = body;
            obj.type = "OTP Verification";
            obj.message = "OTP Verification SMS sent to " + objParam.phone;
            if (flgSendSms) {
                // common.SendSMS(obj);
                return callback({
                    success: true,
                    message: "Verification OTP has been sent to your Mobile No !!",
                })
            }
            else {
                return callback({
                    success: false,
                    message: "Cannot Send Verification OTP. Please try again later !!",
                })
            }
        } else {
            return callback({
                success: false,
                message: "SMS Template Not Found !!"
            })
        }
    })
}

module.exports = {
    SendOTPForVerification: SendOTPForVerification
}
