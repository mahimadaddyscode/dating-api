"use strict";
var jwt = require("jwt-simple");
var TokenKey = process.env.tokenkey;
var moment = require("moment");
const User = require('../models/tbluser');

function CurrentTime(dateToSet, flg) {
    if (dateToSet != null && dateToSet != undefined) {
        switch (flg) {
            case "start":
                dateToSet.setHours(0);
                dateToSet.setMinutes(0);
                dateToSet.setSeconds(0);
                break;
            case "end":
                dateToSet.setHours(23);
                dateToSet.setMinutes(59);
                dateToSet.setSeconds(59);
                break;
        }
        return moment(dateToSet).utc().format("x");
    }
    return moment().utc().format("x");
}

var GetNameFromDate = function (callback) {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();

    var seconds = d.getSeconds();
    var minutes = d.getMinutes();
    var hour = d.getHours();

    var milisec = d.getMilliseconds();
    return callback(
        curr_year.toString() +
        curr_month.toString() +
        curr_date.toString() +
        hour.toString() +
        minutes.toString() +
        seconds.toString() +
        milisec.toString()
    );
};

var GenerateOTP = function () {
    return Math.floor(100000 + Math.random() * 900000);
};

var GenerateUserId = function (callback) {
    var username = Math.floor(10000000 + Math.random() * 90000000).toString();
    User.findOne({
        username: username,
    }).then(function (objUserExist) {
        if (objUserExist != null) {
            return GenerateUserId(function (ress) { });
        } else {
            return callback(username);
        }
    });
};

var SendSMS = function (objSMS) {
    var url = process.env.url;

    var obj = new Object();
    obj.user = process.env.user;
    obj.password = process.env.password;
    obj.sid = process.env.senderid;
    obj.msisdn = objSMS.number;
    obj.msg = objSMS.body;
    obj.fl = 0;
    obj.gwid = 2;

    // console.log("obj", obj)
    superagent.get(url).query(obj).end(function (err123, res123) {
        if (res123 != null && res123 != undefined && res123.status == 200) {
            var obj = new Object();
            obj.smstype = objSMS.type;
            obj.number = objSMS.number;
            obj.message = objSMS.message;
            obj.createddate = CurrentTime();
            SMSLog.create(obj).then(function (resMailLog) { })
        } else {
            console.log("err123 ", err123)
        }
    });
}

var GetUserNameFromDateWithCallback = function (callback) {
    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1; //Months are zero based
    var curr_year = d.getFullYear();

    var seconds = d.getSeconds();
    var minutes = d.getMinutes();
    var hour = d.getHours();

    var milisec = d.getMilliseconds();
    return callback(curr_year.toString() + curr_month.toString() + curr_date.toString() + hour.toString() + minutes.toString() + seconds.toString() + milisec.toString());
}

module.exports = {
    CurrentTime: CurrentTime,
    GetNameFromDate: GetNameFromDate,
    GenerateOTP: GenerateOTP,
    GenerateUserId: GenerateUserId,
    SendSMS: SendSMS,
    GetUserNameFromDateWithCallback: GetUserNameFromDateWithCallback
}