"use strict";
const User = require("../models/tbluser");
var common = require("./common");

const ObjectId = require('mongodb').ObjectID;
var formidable = require('formidable');
var fs = require('fs');
var GetAllUsers = function (objParams, callback) {
    User.find().then(function (resUsers) {
        if (resUsers.length > 0) {
            return callback({
                success: true,
                message: "Records Found !!",
                data: resUsers
            })
        }
        else {
            return callback({
                success: false,
                message: "No Records Found !!"
            })
        }
    })
}

var GetUsersById = function (objParams, callback) {
    User.findOne({
        id: objParams.id
    }).then(function (resUsers) {
        if (resUsers.length > 0) {
            return callback({
                success: true,
                message: "Records Found !!",
                data: resUsers
            })
        }
        else {
            return callback({
                success: false,
                message: "No Records Found !!"
            })
        }
    })
}

var SaveUser = function (objUser, callback) {
    if (objUser.id && objUser.id != null && objUser.id != undefined && objUser.id != 0) {
        User.findOne({
            id: objUser.id,
        }).then(function (objUserExist) {
            if (objUserExist != null) {
                if (objUser.dob != null && objUser.dob != '' && objUser.dob != undefined) {
                    objUser.dob = common.common.CurrentTime(objUser.dob);
                }
                else {
                    objUser.dob = '';
                }
                User.updateOne({
                    id: objUser.id,
                },
                    objUser
                ).then(function (response) {
                    return callback({
                        success: true,
                        message: "Profile Updated Successfully !!",
                        userid: objUser.id,
                    });
                });
            } else {
                return callback({
                    success: false,
                    message: "User Id doesn't exist",
                });
            }
        })
            .catch(function (err) {
                console.error(
                    "[" + moment().format("DD/MM/YYYY hh:mm:ss a") + "] " + err.stack ||
                    err.message
                );
                return callback({
                    success: false,
                    message: err.message,
                });
            });
    }
    else {
        objUser.phone = "91" + objUser.phone;
        if (objUser.password != null && objUser.password != undefined && objUser.password != "") {
            objUser.password = objUser.password.trim();
        }
        if (objUser.email != null && objUser.email != undefined && objUser.email != "") {
            objUser.email = objUser.email.trim();
        }

        objUser.encryptPassword = jwt.encode(objUser.password, TokenKey);
        objUser.password = objUser.encryptPassword;
        objUser.createddate = common.common.CurrentTime();
        if (objUser.dob != null && objUser.dob != '' && objUser.dob != undefined) {
            objUser.dob = common.common.CurrentTime(objUser.dob);
        }
        else {
            objUser.dob = '';
        }

        common.GenerateUserId(function (resusername) {
            objUser.username = resusername;
            User.create(objUser).then(async function (resCreate) {
                if (resCreate) {
                    return callback({
                        success: true,
                        message: "User Created Successfully !!",
                        data: resCreate,
                    });
                }
                else {
                    return callback({
                        success: true,
                        message: "Cannot Create User !!",
                    });
                }
            });
        });
    }
};

var updateUser = function (tokenUserData, objUser, callback) {
    User.findOne({
        _id: ObjectId(tokenUserData._id),
    }).then(function (objUserExist) {
        if (objUserExist != null) {
            if (objUser.dob != null && objUser.dob != '' && objUser.dob != undefined) {
                objUser.dob = common.CurrentTime(objUser.dob);
            }
            else {
                objUser.dob = '';
            }
            User.findOneAndUpdate({
                _id: ObjectId(tokenUserData._id),
            },
                objUser,
                {
                    new: true
                }).then(function (response) {
                    let newUserObj = response.toObject();
                    delete newUserObj.password;
                    delete newUserObj._id;
                    return callback({
                        success: true,
                        msg: "Profile Updated Successfully.",
                        data: newUserObj
                    }, 200);
                });
        } else {
            return callback({
                success: false,
                msg: "User doesn't exist.",
            }, 404);
        }
    }).catch(function (err) {
        return callback({
            msg: err.message,
        }, 500);
    });
}

var uploadImage = function (tokenUserData, req, callback) {
    console.log("tokenUserData", tokenUserData)
    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../mediaupload/profilepic';
    var FileName = [];
    var lstUser = [];

    //file upload path
    form.parse(req, function (err, fields, files) {
        //you can get fields here
    });
    form.on('fileBegin', function (name, file) {
        var ext = file.name.substring(file.name.lastIndexOf('.'), file.name.length);
        common.GetUserNameFromDateWithCallback(function (resName) {
            var NewName = resName + 2;
            file.path = form.uploadDir + '/' + NewName + ext;
            FileName.push(NewName + ext);
            var objName = name.split(',');
            var obj = new Object();
            obj.userid = tokenUserData._id;
            obj.filename = NewName + ext;
            lstUser.push(obj);
        });

        //modify file path
    });
    form.on('end', function () {
        if (lstUser.length > 0) {
            User.findOne({
                _id: ObjectId(lstUser[0].userid),
            }).then(function (resUser) {
                if (resUser.image != '' && resUser.image != null) {
                    var oldFile =
                        __dirname + '/../mediaupload/profilepic/' + resUser.image;
                    fs.exists(oldFile, function (exists) {
                        if (exists) {
                            fs.unlinkSync(oldFile);
                        }
                    });
                }
                User.findOneAndUpdate(
                    {
                        _id: ObjectId(lstUser[0].userid),
                    },
                    {
                        $set: {
                            profilepic: lstUser[0].filename,
                        },
                    },
                    {
                        new: true
                    }).then(function (resUpdate) {
                        return callback({
                            success: true,
                            msg: 'Image uploaded successfully.',
                            data: {
                                UserImage: lstUser[0].filename,
                            },
                        }, 200);
                    }).catch(function (err) {
                        return callback({
                            success: false,
                            msg: err.message
                        }, 500);
                    });
            }).catch(function (err) {
                return callback({
                    success: false,
                    msg: err.message,
                }, 500);
            });
        } else {
            return callback({
                msg: 'Select profile image.',
            }, 422);
        }
    });
}

module.exports = {
    GetAllUsers: GetAllUsers,
    GetUsersById: GetUsersById,
    SaveUser: SaveUser,
    updateUser: updateUser,
    uploadImage: uploadImage
}