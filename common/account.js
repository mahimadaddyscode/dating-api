"use strict";
const User = require("../models/tbluser");
var common = require("../common");


module.exports.ChangePassword = function (objData, callback) {
    // objData.oldpassword = jwt.encode(objData.oldpassword, TokenKey);

    if (objData.password != objData.confirmpassword) {
        return callback({
            success: false,
            message: "Password and Confirm Password does not match !!",
        });
    } else if (objData.password.length < 3) {
        return callback({
            success: false,
            message: "Password must contain atleast 3 characters !!",
        });
    } else {
        User.findOne({
            phone: objData.phone,
        }).then(function (chkUserExist) {
            if (chkUserExist != null) {
                var OldPasswordVerify = bcrypt.compareSync(objData.oldpassword, chkUserExist.password);
                if (OldPasswordVerify) {
                    objData.password = bcrypt.hashSync(objData.password, 5);
                    User.updateOne({
                        phone: objData.phone
                    }, {
                        password: objData.password
                    })
                        .then(function (response) {
                            return callback({
                                success: true,
                                message: "Password Changed Successfully !!",
                            });
                        })
                        .catch(function (error) {
                            return callback({
                                success: false,
                                message: error.message + " !!",
                            });
                        });
                } else {
                    return callback({
                        success: false,
                        message: "Old Password is Wrong !!",
                    });
                }
            } else {
                return callback({
                    success: false,
                    message: "User Does Not Exist !!",
                });
            }
        });
    }
};
