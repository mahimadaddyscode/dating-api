'use srict'
var u = require('underscore');
let trim = (x) => {
  let value = String(x)
  return value.replace(/^\s+|\s+$/gm, '')
}
let isEmpty = (value) => {
  if (value === null || value === undefined || trim(value) === '' || value.length === 0) {
    return true
  } else {
    return false
  }
}

let convertToString = (value, flg) => {
  if (flg == 'array') {
    u.filter(value, obj => {
      for (const prop in obj) {
        if (obj[prop] == null) {
          obj[prop] = "";
        }
      }
    })
    return value;
  } else if (flg == 'obj') {
    for (const prop in value) {
      if (value[prop] == null) {
        value[prop] = "";
      }
    }
    return value;
  } else {
    if (value == null) {
      value = "";
    }
    return value;
  }
}

/**
 * exporting functions.
 */
module.exports = {
  isEmpty: isEmpty,
  convertToString: convertToString
}
