var express = require('express');
var router = express.Router();
var moment = require('moment');
var common = require('../common/common');
var user = require('../common/user');
var smsmail = require('../common/smsmail');
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var TokenKey = process.env.tokenkey;
const User = require('../models/tbluser');
const UserVerification = require('../models/tbluserotpverification');
var Auth = require('../middleware/auth');
var QRCode = require("qrcode");
var formidable = require('formidable');
var fs = require('fs');
var jwt = require('jsonwebtoken');


// Verifying OTP for new New User
router.post('/SendOTPForPhoneVerification', jsonParser, function (req, res) {
    var objParam = req.body;
    User.findOne({
        where: {
            phone: objParam.phone
        }
    }).then(function (objUser) {
        if (objUser != null) {
            res.json({
                success: false,
                message: "Phone Number already exists."
            });
        } else {
            var today = common.CurrentTime();
            var objPassword = new Object();
            objPassword.date = common.CurrentTime();
            objPassword.phone = objParam.phone;
            objPassword.otp = common.GenerateOTP();
            objPassword.expirydate = (parseInt(today) + 30 * 60000);
            UserVerification.findOne({
                phone: objParam.phone,
            }).then((OTPSent) => {
                if (OTPSent) {
                    objPassword.otp = OTPSent.otp;
                    smsmail.SendOTPForVerification(objPassword, function (resMail) {
                        res.json({
                            success: true,
                            message: "OTP is sent to your Mobile number .",
                            username: objParam.phone,
                        });
                    });
                } else {
                    UserVerification.create(objPassword).then(function (resOTP) {
                        smsmail.SendOTPForVerification(objPassword, function (resMail) {
                            res.json({
                                success: true,
                                message: "OTP is sent to your Mobile number .",
                                username: objParam.phone,
                            });
                        });
                    });
                }
            });
        }
    })
});

//Verify OTP Send to Login and Register both
router.post("/CheckOTPForLogin", jsonParser, function (req, res) {
    var objParam = req.body;

    UserVerification.aggregate([
        {
            $match: {
                otp: objParam.OTP,
                phone: objParam.phone
            },
        },
        {
            $lookup: {
                from: "tblusers",
                localField: "phone",
                foreignField: "phone",
                as: "tbluser",
            },
        },
        {
            $unwind: {
                path: "$tbluser",
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $match: {
                "tbluser.phone": objParam.phone
            },
        },
    ]).then(function (resExist) {
        if (resExist.length > 0 && resExist != null) {
            var today = common.CurrentTime();
            var expiryDate = resExist[0].expirydate;
            if (today < expiryDate) {
                if (objParam.phone) {
                    var user = {
                        _id: response[0]._id,
                        uniqueid: response[0].uniqueid,
                        profileid: response[0].profileid,
                        phone: response[0].phone,
                    };
                    jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                        res.json({
                            success: true,
                            token: "JWT " + token,
                            UserId: resExist[0].tbluser.id,
                            UserName: resExist[0].tbluser.uniqueid,
                            fullname: resExist[0].tbluser.fullname,
                            Email: resExist[0].tbluser.email,
                            Phone: resExist[0].tbluser.phone,
                            message: "Login Successfully...",
                        });
                        User.findOneAndUpdate({
                            phone: resExist[0].tbluser.phone,
                            uniqueid: resExist[0].tbluser.uniqueid,
                            $set: {
                                accesstoken: token
                            }
                        }).then(function (response) {

                        })

                    })
                }

                UserVerification.remove({
                    phone: resExist[0].tbluser.phone,
                });
            } else {
                res.json({
                    success: false,
                    message: "Invalid OTP",
                });
            }
        } else {
            const errorMsg = objParam.phone
                ? "Invalid OTP or phone"
                : "Invalid OTP or username";
            res.json({
                success: false,
                message: errorMsg,
            });
        }
    });
});

router.post("/VerifyOTP", jsonParser, function (req, res) {
    var objParam = req.body;
    UserVerification.findOne({
        phone: objParam.phone,
    }).then((OTPExist) => {
        if (OTPExist) {
            var today = common.CurrentTime();
            var expiryDate = OTPExist.expirydate;
            if (today < expiryDate) {
                if (objParam.otp == OTPExist.otp) {
                    UserVerification.remove({
                        phone: objParam.phone,
                    }).then(function (resOTP) {
                        res.json({
                            success: true,
                            message: "OTP verified.",
                            user: OTPExist,
                        });
                    });
                } else {
                    res.json({
                        success: false,
                        message: "Invalid OTP",
                    });
                }
            } else {
                UserVerification.remove({
                    phone: objParam.phone,
                }).then(function (resOTP) {
                    res.json({
                        success: false,
                        message: "OTP expired",
                    });
                });
            }
        } else {
            res.json({
                success: false,
                message: "Invalid Phone number.",
            });
        }
    });
});

//Look from this Example
router.post("/CheckOTP", jsonParser, function (req, res) {
    var objParam = req.body;
    const findQuery = objParam.phone
        ? {
            "tbluser.phone": "91" + objParam.phone,
        }
        : {
            "tbluser.username": objParam.username,
        };

    ResetPW.aggregate([
        {
            $match: {
                otp: objParam.OTP,
            },
        },
        {
            $lookup: {
                from: "tblusers",
                localField: "userid",
                foreignField: "id",
                as: "tbluser",
            },
        },
        {
            $unwind: {
                path: "$tbluser",
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: "tblroles",
                localField: "tbluser.roleid",
                foreignField: "id",
                as: "tbluser.tblrole",
            },
        },
        {
            $unwind: {
                path: "$tbluser.tblrole",
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $match: findQuery,
        },
    ]).then(function (resExist) {
        if (resExist.length > 0 && resExist != null) {
            var today = common.CurrentTime();
            var expiryDate = resExist[0].expirydate;
            if (today < expiryDate) {
                if (objParam.phone) {
                    var user = {
                        phone: objParam.phone,
                        Role: resExist[0].tbluser.tblrole.rolename,
                        RoleType: resExist[0].tbluser.tblrole.type,
                    };
                    var token = jwt.encode(user, TokenKey);
                    res.json({
                        success: true,
                        token: "JWT " + token,
                        UserId: resExist[0].tbluser.id,
                        UserName: resExist[0].tbluser.username,
                        UserImage: resExist[0].tbluser.image,
                        UserVerified: resExist[0].tbluser.isverified,
                        UserActiveStatus: resExist[0].tbluser.isactivated,
                        UserDocVerify: 1,
                        fullname: resExist[0].tbluser.fullname,
                        Email: resExist[0].tbluser.email,
                        Phone: resExist[0].tbluser.phone,
                        UserImage: resExist[0].tbluser.image,
                        UserRole: user.Role,
                        UserRoleType: user.RoleType,
                        message: "Login Successfully...",
                    });
                }

                ResetPW.remove({
                    userid: resExist[0].tbluser.id,
                });
            } else {
                res.json({
                    success: false,
                    message: "Invalid OTP",
                });
            }
        } else {
            const errorMsg = objParam.phone
                ? "Invalid OTP or phone"
                : "Invalid OTP or username";
            res.json({
                success: false,
                message: errorMsg,
            });
        }
    });
});

//Update User
router.post("/updateUser", Auth.verifyToken, jsonParser, function (req, res) {
    let objUser = req.body;

    user.updateUser(objUser.userData, objUser, function (response, code) {
        res.status(code).json(response);
    });
});

//Upload Profile Picture
router.post('/uploadImage', Auth.verifyToken, function (req, res) {
    console.log("req.body", req.body)
    let tokenUserData = req.body.userData;

    user.uploadImage(tokenUserData, req, function (response, code) {
        res.status(code).json(response);
    });
});

// Register new User
router.post('/Register', jsonParser, function (req, res) {
    var objParam = req.body;
    var form = new formidable.IncomingForm();
    form.uploadDir = __dirname + '/../mediaupload/profilepic';

    var objFile;

    form.parse(req, function (err, fields, files) {
        objParam = JSON.parse(fields.objUser);
    });

    form.on('fileBegin', function (name, file) {
        var ext = file.name.substring(file.name.indexOf('.'), file.name.length);
        common.GetUserNameFromDateWithCallback(function (resName) {
            var NewName = resName + 2;
            NewName = "B" + NewName + ext;
            file.path = form.uploadDir + "/" + NewName;
            objFile = NewName;
        });
    });

    form.on('end', function () {
        if (objParam.fullname && objParam.fullname != '' && objParam.uniqueid && objParam.uniqueid != '') {
            User.findOne({ $or: [{ uniqueid: objParam.uniqueid }, { phone: objParam.phone }] }).then(function (resUser) {
                console.log("resUser", resUser)
                if (resUser != null) {
                    console.log("resUser.phone", resUser)
                    if (objParam.phone == resUser.phone) {
                        res.json({
                            success: false,
                            message: "Phone number is already used by another User",
                        });
                    } else {
                        res.json({
                            success: false,
                            message: "Uniqueid is already used by another User",
                        });
                    }
                } else {
                    objParam.createddate = common.CurrentTime();
                    common.GetUserNameFromDateWithCallback(function (resName) {
                        console.log("resName", resName);
                        var NewName = resName + 2 + ".svg";
                        var QRdata = {
                            fullname: objParam.fullname,
                            phone: objParam.phone,
                            uniqueid: objParam.uniqueid,
                        };
                        console.log("QRdata", QRdata);
                        jwt.sign({ QRdata }, TokenKey, (err, token) => {
                            // var token = jwt.encode(QRdata, TokenKey);

                            QRCode.toFile(
                                "./mediaupload/qrcode/" + NewName,
                                token, {
                                type: "svg",
                            },
                                function (err) {
                                    if (err) {
                                        throw err;
                                    }
                                    console.log("QR Generated.");
                                    objParam.qrimage = NewName;
                                    console.log("objParam", objParam)
                                    if (objFile && objFile != undefined) {
                                        objParam.profilepic = objFile;
                                    }
                                    User.create(objParam).then(function (resUser) {
                                        console.log("fdoneemee")
                                        res.json({
                                            success: true,
                                            message: "User created successfully...",
                                            data: resUser
                                        });
                                    }).catch((err) => {
                                        res.json({
                                            success: false,
                                            message: "Something went wrong..."
                                        });
                                    })
                                });
                        });

                    });
                }
            })
        } else {
            res.json({
                success: false,
                message: "Required Parameter password and fullname is missing",
            });
        }
    })
});

//Check Unique ID of User
router.post('/CheckUniqueidExist', jsonParser, function (req, res) {
    var objParam = req.body;

    User.findOne({ uniqueid: objParam.uniqueid }).then(function (resExist) {
        if (resExist != null) {
            res.json({
                success: true,
                message: "User Found",
                data: resExist
            });

        } else {
            res.json({
                success: false,
                message: "User Not Found",
                data: []
            });
        }
    })
});

//new
router.post("/SendLoginOTP", jsonParser, function (req, res) {
    objUser = req.body;
    // account.SendLoginOTP(objUser, function (respas) {
    //     res.json(respas);
    // });
    var today = common.CurrentTime();
    var objPassword = new Object();
    objPassword.phone = "91" + objParam.phone
    objPassword.otp = common.GenerateOTP();
    objPassword.date = common.CurrentTime();
    objPassword.expirydate = parseInt(objPassword.date) + 30 * 60000;
    UserVerification.findOneAndUpdate({
        phone: "91" + objPassword.phone
    }, {
        $setOnInsert: objPassword
    }, {
        returnOriginal: false,
        upsert: true,
    }).then(function (resVerify) {
        if (!resVerify) {
            smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                res.json(resSMS);
            }
            );
        }
        else {
            if (today < resVerify.expirydate) {
                objPassword = resVerify;
                smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                    res.json(resSMS);
                }
                );
            }
            else {
                objPassword._id = ObjectId(resVerify._id);
                UserVerification.updateOne({
                    phone: "91" + objPassword.phone
                }, {
                    $set: {
                        otp: objPassword.otp,
                        expirydate: objPassword.expirydate,
                    },
                }).then(function (resUpdate) {
                    smsmail.SendOTPForVerification(objParam, objPassword, true, function (resSMS) {
                        res.json(resSMS);
                    });
                });
            }
        }
    })
});

//new
router.post("/CheckLoginOTP", jsonParser, function (req, res) {
    var objParam = req.body;

    UserVerification.aggregate([{
        $match: {
            otp: objParam.OTP.toString(),
            phone: "91" + objParam.phone
        },
    },
    ]).then(function (resExist) {
        if (resExist.length > 0 && resExist != null) {
            var today = common.CurrentTime();
            var expiryDate = resExist[0].expirydate;
            if (today < expiryDate) {
                User.aggregate([{
                    $match: {
                        phone: "91" + objParam.phone
                    }
                },
                { $set: { roleid: { $toObjectId: "$roleid" } } },
                {
                    $lookup: {
                        from: "tblroles",
                        localField: "roleid",
                        foreignField: "_id",
                        as: "tblrole",
                    },
                },
                {
                    $unwind: {
                        path: "$tblrole",
                        preserveNullAndEmptyArrays: true,
                    },
                },
                ]).then(function (resUser) {
                    if (resUser.length > 0 && resUser != null) {
                        var user = {
                            _id: resUser[0]._id,
                            phone: resUser[0].phone,
                            email: resUser[0].email,
                            Role: resUser[0].tblrole.rolename,
                            RoleType: resUser[0].tblrole.roletype,
                        };
                        jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                            res.json({
                                success: true,
                                userexists: true,
                                token: token,
                                UserId: resUser[0]._id,
                                FullName: resUser[0].fullname,
                                Email: resUser[0].email,
                                Phone: resUser[0].phone,
                                UserImage: resUser[0].image,
                                UserRole: resUser[0].tblrole.rolename,
                                UserRoleType: resUser[0].tblrole.roletype,
                                message: "Login Successfully !!",
                            });
                        });
                        LoginVerify.remove({
                            phone: resUser[0].phone,
                        });
                    }
                    else {
                        Role.findOne({
                            roletype: "User"
                        }).then(function (resRole) {
                            objParam.roleid = resRole._id;
                            objParam.createddate = common.CurrentTime();
                            if (objParam.phone != null && objParam.phone != undefined && objParam.phone != '') {
                                objParam.phone = "91" + objParam.phone
                            }
                            User.create(objParam).then(function (resUserCreated) {
                                if (resUserCreated) {
                                    var user = {
                                        _id: resUserCreated._id,
                                        phone: resUserCreated.phone,
                                        email: resUserCreated.email,
                                        Role: resRole.rolename,
                                        RoleType: resRole.roletype,
                                    };
                                    jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                                        res.json({
                                            success: true,
                                            userexists: true,
                                            token: token,
                                            UserId: resUserCreated._id,
                                            FullName: resUserCreated.fullname,
                                            Email: resUserCreated.email,
                                            Phone: resUserCreated.phone,
                                            UserImage: resUserCreated.image,
                                            UserRole: resRole.rolename,
                                            UserRoleType: resRole.roletype,
                                            message: "Login Successfully !!",
                                        });
                                    });
                                    LoginVerify.remove({
                                        phone: objParam.phone,
                                    });
                                }
                                else {
                                    res.json({
                                        success: false,
                                        message: "Something went wrong. Please try again later !!",
                                    });
                                }
                            })
                        })
                    }
                })
            }
            else {
                res.json({
                    success: false,
                    message: "Invalid OTP !!",
                });
            }
        }
        else {
            res.json({
                success: false,
                message: "Invalid OTP or Phone No !!",
            });
        }
    });
});

//new
router.post("/LoginWithGmailorFB", jsonParser, function (req, res) {
    var objParam = req.body;

    User.aggregate([{
        $match: {
            $or: [
                {
                    profileid: objParam.profileid
                },
                {
                    email: objParam.email.trim()
                },
            ],
        }
    },
    { $set: { roleid: { $toObjectId: "$roleid" } } },
    {
        $lookup: {
            from: "tblroles",
            localField: "roleid",
            foreignField: "_id",
            as: "tblrole",
        },
    },
    {
        $unwind: {
            path: "$tblrole",
            preserveNullAndEmptyArrays: true,
        },
    },
    ]).then(function (resUser) {
        if (resUser.length > 0 && resUser != null) {
            var user = {
                _id: resUser[0]._id,
                phone: resUser[0].phone,
                email: resUser[0].email,
                profileid: resUser[0].profileid,
                Role: resUser[0].tblrole.rolename,
                RoleType: resUser[0].tblrole.roletype,
            };
            jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                res.json({
                    success: true,
                    userexists: true,
                    token: token,
                    UserId: resUser[0]._id,
                    FullName: resUser[0].fullname,
                    Email: resUser[0].email,
                    Phone: resUser[0].phone,
                    UserImage: resUser[0].image,
                    UserRole: resUser[0].tblrole.rolename,
                    UserRoleType: resUser[0].tblrole.roletype,
                    message: "Login Successfully !!",
                });
                User.findOneAndUpdate({
                    profileid: resUser[0].profileid,
                    uniqueid: resUser[0].uniqueid,
                    $set: {
                        accesstoken: token
                    }
                }).then(function (response) {

                })
            });
            UserVerification.remove({
                phone: resUser[0].phone,
            });
        }
        else {
            Role.findOne({
                roletype: "User"
            }).then(function (resRole) {
                objParam.roleid = resRole._id;
                objParam.createddate = common.CurrentTime();
                if (objParam.phone != null && objParam.phone != undefined && objParam.phone != '') {
                    objParam.phone = "91" + objParam.phone
                }
                User.create(objParam).then(function (resUserCreated) {
                    if (resUserCreated) {
                        var user = {
                            _id: resUserCreated._id,
                            phone: resUserCreated.phone,
                            email: resUserCreated.email,
                            Role: resRole.rolename,
                            RoleType: resRole.roletype,
                        };
                        jwt.sign({ user }, TokenKey, { expiresIn: '365d' }, (err, token) => {
                            res.json({
                                success: true,
                                userexists: false,
                                token: token,
                                UserId: resUserCreated._id,
                                FullName: resUserCreated.fullname,
                                Email: resUserCreated.email,
                                Phone: resUserCreated.phone,
                                UserImage: resUserCreated.image,
                                UserRole: resRole.rolename,
                                UserRoleType: resRole.roletype,
                                message: "Login Successfully !!",
                            });
                        });
                        UserVerification.remove({
                            phone: objParam.phone,
                        });
                    }
                    else {
                        res.json({
                            success: false,
                            message: "Something went wrong. Please try again later !!",
                        });
                    }
                })
            })
        }
    })

});

module.exports = router;